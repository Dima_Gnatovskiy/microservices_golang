package main

import (
	pb "bitbucket.org/Dima_Gnatovskiy/microservices_golang/consignment-service/proto/consignment"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type repository interface {
	Create(consignment *pb.Consignment) error
	GetAll() ([]*pb.Consignment, error)
}

type MongoRepository struct {
	collection *mongo.Collection
}

func (repository *MongoRepository) Create(consignment *pb.Consignment) error {
	_, err := repository.collection.InsertOne(context.Background(), consignment)
	return err
}

func (repository *MongoRepository) GetAll() ([]*pb.Consignment, error) {
	cur, err := repository.collection.Find(context.Background(), bson.D{})
	if err != nil {
		return nil, err
	}

	var consignments []*pb.Consignment
	for cur.Next(context.Background()) {
		var consignment *pb.Consignment
		err := cur.Decode(&consignment)
		if err != nil {
			return nil, err
		}
		consignments = append(consignments, consignment)
	}

	return consignments, nil
}
