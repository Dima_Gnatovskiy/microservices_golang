module bitbucket.org/Dima_Gnatovskiy/microservices_golang/consignment-service

go 1.12

require (
	bitbucket.org/Dima_Gnatovskiy/microservices_golang/user-service v0.0.0-20190804195220-dbda432723c2
	bitbucket.org/Dima_Gnatovskiy/microservices_golang/vessel-service v0.0.0-20190804180931-c27d9d75f6f6
	github.com/golang/protobuf v1.3.2
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/json-iterator/go v1.1.7 // indirect
	github.com/micro/go-micro v1.8.1
	github.com/nats-io/jwt v0.2.12 // indirect
	github.com/tidwall/pretty v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.0.4
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	golang.org/x/sys v0.0.0-20190801041406-cbf593c0f2f3 // indirect
	google.golang.org/genproto v0.0.0-20190716160619-c506a9f90610 // indirect
)
