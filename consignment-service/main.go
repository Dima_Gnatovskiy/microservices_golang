package main

import (
	pb "bitbucket.org/Dima_Gnatovskiy/microservices_golang/consignment-service/proto/consignment"
	userProto "bitbucket.org/Dima_Gnatovskiy/microservices_golang/user-service/proto/user"
	vesselProto "bitbucket.org/Dima_Gnatovskiy/microservices_golang/vessel-service/proto/vessel"
	"context"
	"errors"
	"fmt"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-micro/metadata"
	"github.com/micro/go-micro/server"
	"log"
	"os"
	"sync"
)

const (
	defaultStoreHost = "mongodb://localhost:27017"
)

type Repository struct {
	mu           sync.RWMutex
	consignments []*pb.Consignment
}

func main() {

	// Set-up micro instance
	srv := micro.NewService(
		// This name must match the package name given in your protobuf definition
		micro.Name("shippy.service.consignment"),
		micro.WrapHandler(AuthWrapper),
	)

	srv.Init()

	uri := os.Getenv("DB_HOST")
	if uri == "" {
		uri = defaultStoreHost
	}
	client, err := CreateClient(uri)
	if err != nil {
		log.Panicf("Could not crate store client &v", err)
	}
	defer client.Disconnect(context.TODO())

	consignmentCollection := client.Database("shippy").Collection("consignments")

	mongoRepository := &MongoRepository{collection: consignmentCollection}
	vesselClient := vesselProto.NewVesselService("shippy.service.vessel", srv.Client())
	h := &handler{mongoRepository, vesselClient}

	// Registry handler
	pb.RegisterShippingServiceHandler(srv.Server(), h)

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}

func AuthWrapper(fn server.HandlerFunc) server.HandlerFunc {
	return func(ctx context.Context, req server.Request, res interface{}) error {

		meta, ok := metadata.FromContext(ctx)
		if !ok {
			return errors.New("no auth meta-data found in request")
		}

		token := meta["Token"]
		log.Println("Authenticating with token: ", token)

		authClient := userProto.NewUserService("shippy.service.user", client.DefaultClient)
		_, err := authClient.ValidateToken(ctx, &userProto.Token{
			Token: token,
		})

		if err != nil {
			log.Printf("could not validate token %v", err)
			return err
		}

		return fn(ctx, req, res)
	}
}
