package main

import (
	pb "bitbucket.org/Dima_Gnatovskiy/microservices_golang/consignment-service/proto/consignment"
	vesselPb "bitbucket.org/Dima_Gnatovskiy/microservices_golang/vessel-service/proto/vessel"
	"context"
)

type handler struct {
	repository   repository
	vesselClient vesselPb.VesselService
}

func (h *handler) CreateConsignment(ctx context.Context, req *pb.Consignment, res *pb.Response) error {

	vesselRes, err := h.vesselClient.FindAvailable(ctx, &vesselPb.Specification{
		Capacity:  int32(len(req.Containers)),
		MaxWeight: req.Weight,
	})
	if err != nil {
		return err
	}

	req.VesselId = vesselRes.Vessel.Id

	if err := h.repository.Create(req); err != nil {
		return err
	}

	res.Created = true
	res.Consignment = req

	return nil
}

func (h *handler) GetConsignments(ctx context.Context, req *pb.GetRequest, res *pb.Response) error {
	consignments, err := h.repository.GetAll()
	if err != nil {
		return err
	}
	res.Consignments = consignments
	return nil
}
