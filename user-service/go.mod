module bitbucket.org/Dima_Gnatovskiy/microservices_golang/user-service

go 1.12

require (
	cloud.google.com/go v0.43.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/golang/protobuf v1.3.2
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/jinzhu/gorm v1.9.10
	github.com/json-iterator/go v1.1.7 // indirect
	github.com/lib/pq v1.2.0 // indirect
	github.com/micro/go-micro v1.8.1
	github.com/micro/protoc-gen-micro v0.8.0 // indirect
	github.com/nats-io/jwt v0.2.12 // indirect
	github.com/nats-io/nats-server/v2 v2.0.2 // indirect
	github.com/satori/go.uuid v1.2.0
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	golang.org/x/sys v0.0.0-20190726091711-fc99dfbffb4e // indirect
	google.golang.org/grpc v1.22.1 // indirect
)
