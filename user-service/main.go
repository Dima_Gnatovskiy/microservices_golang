package main

import (
	pb "bitbucket.org/Dima_Gnatovskiy/microservices_golang/user-service/proto/user"
	"github.com/micro/go-micro"
	"log"
)

func main() {

	db, err := CreateConnection()
	if err != nil {
		log.Panicf("could not create connection %v", err)
	}

	defer db.Close()

	db.AutoMigrate(&pb.User{})

	userRepository := &UserRepository{db: db}

	tokenService := &TokenService{userRepository}

	srv := micro.NewService(
		micro.Name("shippy.service.user"),
	)

	srv.Init()

	pubsub := micro.NewPublisher("user.created", srv.Client())

	pb.RegisterUserServiceHandler(srv.Server(), &handler{
		repo:         userRepository,
		tokenService: tokenService,
		pubsub:       pubsub,
	})

	err = srv.Run()
	if err != nil {
		log.Fatalf("could not run server %v", err)
	}
}
