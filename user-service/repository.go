package main

import (
	pb "bitbucket.org/Dima_Gnatovskiy/microservices_golang/user-service/proto/user"
	"github.com/jinzhu/gorm"
)

type repository interface {
	GetAll() ([]*pb.User, error)
	Get(id string) (*pb.User, error)
	Create(user *pb.User) error
	GetByEmailAndPassword(user *pb.User) (*pb.User, error)
	GetByEmail(email string) (*pb.User, error)
}

type UserRepository struct {
	db *gorm.DB
}

func (r *UserRepository) GetAll() ([]*pb.User, error) {
	var users []*pb.User
	if err := r.db.Find(&users).Error; err != nil {
		return nil, err
	}

	return users, nil
}

func (r *UserRepository) Get(id string) (*pb.User, error) {
	var user *pb.User
	user.Id = id
	if err := r.db.First(&user).Error; err != nil {
		return nil, err
	}

	return user, nil
}

func (r *UserRepository) GetByEmailAndPassword(user *pb.User) (*pb.User, error) {
	if err := r.db.First(&user).Error; err != nil {
		return nil, err
	}

	return user, nil
}

func (r *UserRepository) GetByEmail(email string) (*pb.User, error) {
	user := &pb.User{}
	if err := r.db.Where("email = ?", email).
		First(&user).Error; err != nil {
		return nil, err
	}
	return user, nil
}

func (r *UserRepository) Create(user *pb.User) error {
	if err := r.db.Create(user).Error; err != nil {
		return err
	}

	return nil
}
