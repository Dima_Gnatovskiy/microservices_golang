package main

import (
	pb "bitbucket.org/Dima_Gnatovskiy/microservices_golang/user-service/proto/user"
	"context"
	"errors"
	"fmt"
	"github.com/micro/go-micro"
	"golang.org/x/crypto/bcrypt"
	"log"
)

const topic = "user.created"

type handler struct {
	repo         repository
	tokenService authable
	pubsub       micro.Publisher
}

func (h *handler) Get(ctx context.Context, req *pb.User, res *pb.Response) error {
	user, err := h.repo.Get(req.Id)
	if err != nil {
		return err
	}
	res.User = user
	return nil
}

func (h *handler) GetAll(ctx context.Context, req *pb.Request, res *pb.Response) error {
	users, err := h.repo.GetAll()
	if err != nil {
		return nil
	}

	res.Users = users
	return nil
}

func (h *handler) Auth(ctx context.Context, req *pb.User, res *pb.Token) error {
	log.Println("Logging in with:", req.Email, req.Password)
	user, err := h.repo.GetByEmail(req.Email)
	log.Println(user, err)
	if err != nil {
		return err
	}

	// Compares our given password against the hashed password
	// stored in the database
	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password)); err != nil {
		return err
	}

	token, err := h.tokenService.Encode(user)
	if err != nil {
		return err
	}
	res.Token = token
	return nil
}

func (h *handler) Create(ctx context.Context, user *pb.User, res *pb.Response) error {

	log.Println("Creating user: ", user)

	// Generates a hashed version of our password
	hashedPass, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return errors.New(fmt.Sprintf("error hashing password: %v", err))
	}

	user.Password = string(hashedPass)
	if err := h.repo.Create(user); err != nil {
		return errors.New(fmt.Sprintf("error creating user: %v", err))
	}

	res.User = user
	if err := h.pubsub.Publish(ctx, user); err != nil {
		return errors.New(fmt.Sprintf("error publishing event: %v", err))
	}

	return nil
}

func (h *handler) ValidateToken(ctx context.Context, req *pb.Token, res *pb.Token) error {

	// Decode token
	claims, err := h.tokenService.Decode(req.Token)

	if err != nil {
		return err
	}

	if claims.User.Id == "" {
		return errors.New("invalid user")
	}

	res.Valid = true

	return nil
}
