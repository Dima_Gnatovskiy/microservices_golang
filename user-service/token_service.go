package main

import (
	pb "bitbucket.org/Dima_Gnatovskiy/microservices_golang/user-service/proto/user"
	"github.com/dgrijalva/jwt-go"
	"time"
)

var (
	key = []byte("p[]123")
)

type CustomClaims struct {
	User *pb.User
	jwt.StandardClaims
}

type authable interface {
	Decode(token string) (*CustomClaims, error)
	Encode(user *pb.User) (string, error)
}

type TokenService struct {
	repo repository
}

func (t *TokenService) Decode(tokenStr string) (*CustomClaims, error) {
	// Parse the token
	token, err := jwt.ParseWithClaims(tokenStr, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return key, nil
	})

	// Validate the token and return the custom claims
	if claims, ok := token.Claims.(*CustomClaims); ok && token.Valid {
		return claims, nil
	} else {
		return nil, err
	}
}

func (t *TokenService) Encode(user *pb.User) (string, error) {

	expireToken := time.Now().Add(time.Hour * 72).Unix()

	// Create the Claims
	claims := CustomClaims{
		user,
		jwt.StandardClaims{
			ExpiresAt: expireToken,
			Issuer:    "shippy.service.user",
		},
	}

	// Create token
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// Sign token and return
	return token.SignedString(key)
}
