package shippy_service_user

import (
	"github.com/jinzhu/gorm"
	"github.com/satori/go.uuid"
)

func (m *User) BeforeCreate(scope *gorm.Scope) error {
	uuid := uuid.NewV4()
	return scope.SetColumn("Id", uuid.String())
}