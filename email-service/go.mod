module email-service

go 1.12

require (
	bitbucket.org/Dima_Gnatovskiy/microservices_golang/user-service v0.0.0-20190806093647-17039445645c
	github.com/micro/go-micro v1.8.2
)
