module user-cli

go 1.12

require (
	bitbucket.org/Dima_Gnatovskiy/microservices_golang/user-service v0.0.0-20190804195220-dbda432723c2
	cloud.google.com/go v0.43.0 // indirect
	github.com/denisenkom/go-mssqldb v0.0.0-20190724012636-11b2859924c1 // indirect
	github.com/fsouza/go-dockerclient v1.4.2 // indirect
	github.com/gogo/protobuf v1.2.2-0.20190730201129-28a6bbf47e48 // indirect
	github.com/google/pprof v0.0.0-20190723021845-34ac40c74b70 // indirect
	github.com/gorilla/handlers v1.4.2 // indirect
	github.com/hashicorp/golang-lru v0.5.3 // indirect
	github.com/lib/pq v1.2.0 // indirect
	github.com/marten-seemann/qtls v0.3.2 // indirect
	github.com/mattn/go-sqlite3 v1.11.0 // indirect
	github.com/micro/cli v0.2.0
	github.com/micro/go-micro v1.8.1
	github.com/mwitkow/go-conntrack v0.0.0-20190716064945-2f068394615f // indirect
	github.com/nats-io/jwt v0.2.12 // indirect
	github.com/prometheus/client_golang v1.1.0 // indirect
	golang.org/x/exp v0.0.0-20190731235908-ec7cb31e5a56 // indirect
	golang.org/x/image v0.0.0-20190802002840-cff245a6509b // indirect
	golang.org/x/mobile v0.0.0-20190719004257-d2bd2a29d028 // indirect
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80
	golang.org/x/sys v0.0.0-20190804053845-51ab0e2deafa // indirect
	golang.org/x/tools v0.0.0-20190802220118-1d1727260058 // indirect
	google.golang.org/genproto v0.0.0-20190801165951-fa694d86fc64 // indirect
	google.golang.org/grpc v1.22.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.1 // indirect
	gopkg.in/src-d/go-git.v4 v4.13.1 // indirect
	honnef.co/go/tools v0.0.1-2019.2.2 // indirect
)
