module consignment-cli

go 1.12

require (
	bitbucket.org/Dima_Gnatovskiy/microservices_golang/consignment-service v0.0.0-20190804195220-dbda432723c2
	github.com/micro/cli v0.2.0
	github.com/micro/go-micro v1.8.1
)
