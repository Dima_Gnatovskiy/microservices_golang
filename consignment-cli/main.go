package main

import (
	pb "bitbucket.org/Dima_Gnatovskiy/microservices_golang/consignment-service/proto/consignment"
	"context"
	"encoding/json"
	"github.com/micro/cli"
	"github.com/micro/go-micro"
	microclient "github.com/micro/go-micro/client"
	"github.com/micro/go-micro/metadata"
	"io/ioutil"
	"log"
	"os"
)

const (
	address         = "localhost:50051"
	defaultFilename = "consignment.json"
)

func parseFile(filename string) (*pb.Consignment, error) {

	var consignment *pb.Consignment
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(data, &consignment)
	return consignment, err
}

func main() {

	client := pb.NewShippingService("shippy.service.consignment", microclient.DefaultClient)

	service := micro.NewService(
		micro.Flags(
			cli.StringFlag{
				Name:  "filePath",
				Usage: "Provide path to file",
			},
			cli.StringFlag{
				Name:  "token",
				Usage: "Token for user-service",
			},
		),
		micro.Name("shippy.consignment.cli"),
	)

	actionHandler := func(c *cli.Context) {

		filePath := c.String("filePath")
		if filePath == "" {
			filePath = defaultFilename
		}

		token := c.String("token")
		if token == "" {
			log.Fatalf("Please specify token via flag '--token'")
		}

		consignment, err := parseFile(filePath)
		if err != nil {
			log.Fatalf("could not parse file: %s, error: %v ", filePath, err)
		}

		ctx := metadata.NewContext(context.Background(), map[string]string{
			"token": token,
		})

		res, err := client.CreateConsignment(ctx, consignment)
		if err != nil {
			log.Fatalf("Could not create: %v", err)
		}
		log.Printf("Created: %t", res.Created)

		getAll, err := client.GetConsignments(ctx, &pb.GetRequest{})
		if err != nil {
			log.Fatalf("Could not list consignments: %v", err)
		}

		for _, v := range getAll.Consignments {
			log.Println(v)
		}

		os.Exit(0)
	}

	service.Init(
		micro.Action(actionHandler),
	)

	if err := service.Run(); err != nil {
		log.Println(err)
	}
}
