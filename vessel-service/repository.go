package main

import (
	pb "bitbucket.org/Dima_Gnatovskiy/microservices_golang/vessel-service/proto/vessel"
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type repository interface {
	FindAvailable(specification *pb.Specification) (*pb.Vessel, error)
	Create(vessel *pb.Vessel) error
}

type MongoRepository struct {
	collection *mongo.Collection
}

func (mongoRepository *MongoRepository) FindAvailable(specification *pb.Specification) (*pb.Vessel, error) {

	res := mongoRepository.collection.FindOne(context.Background(),
		bson.D{
			{"capacity", bson.D{{"$gte", specification.Capacity}}},
			{"maxweight", bson.D{{"$gte", specification.MaxWeight}}},
		},
	)

	err := res.Err()
	if err != nil {
		return nil, err
	}

	var vessel *pb.Vessel
	err = res.Decode(&vessel)
	if err != nil {
		return nil, err
	}

	return vessel, nil
}

func (mongoRepository *MongoRepository) Create(vessel *pb.Vessel) error {
	_, err := mongoRepository.collection.InsertOne(context.Background(), vessel)
	return err
}
