package main

import (
	pb "bitbucket.org/Dima_Gnatovskiy/microservices_golang/vessel-service/proto/vessel"
	"context"
)

type handler struct {
	repository repository
}

func (h *handler) FindAvailable(ctx context.Context, req *pb.Specification, res *pb.Response) error {
	vessel, err := h.repository.FindAvailable(req)
	if err != nil {
		return err
	}

	res.Vessel = vessel

	return nil
}

func (h *handler) Create(ctx context.Context, req *pb.Vessel, res *pb.Response) error {

	err := h.repository.Create(req)
	if err != nil {
		return err
	}

	res.Created = true
	res.Vessel = req

	return nil
}
