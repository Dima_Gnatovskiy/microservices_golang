package main

import (
	pb "bitbucket.org/Dima_Gnatovskiy/microservices_golang/vessel-service/proto/vessel"
	"context"
	"fmt"
	"github.com/micro/go-micro"
	"log"
	"os"
)

const (
	defaultStoreHost = "mongodb://localhost:27017"
)

func createDummyData(repo repository) {
	vessels := []*pb.Vessel{
		{Id: "vessel001", Name: "Kane's Salty Secret", MaxWeight: 200000, Capacity: 500},
	}
	for _, v := range vessels {
		repo.Create(v)
	}
}

func main() {

	srv := micro.NewService(
		micro.Name("shippy.service.vessel"),
	)

	srv.Init()

	uri := os.Getenv("DB_HOST")
	if uri == "" {
		uri = defaultStoreHost
	}
	client, err := CreateClient(uri)
	if err != nil {
		log.Panicf("Could not crate store client &v", err)
	}
	defer client.Disconnect(context.TODO())

	vesselCollection := client.Database("shippy").Collection("vessel")

	mongoRepository := &MongoRepository{collection: vesselCollection}

	createDummyData(mongoRepository)

	// Register our implementation with
	pb.RegisterVesselServiceHandler(srv.Server(), &handler{repository: mongoRepository})

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
